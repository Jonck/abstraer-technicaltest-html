"use strict";
import { query } from './base/selector';
import { addClass } from './base/classHTML'
import { modal } from './modal/modal';
import carrusel from './carrusel/swiper';

// Start
document.addEventListener('DOMContentLoaded', init);

//function
function init() {
    //loader
    const loader = query('.loder');
    addClass(loader, 'hideLouder');

    //Nav
    const btnNavMobile = query('.btn--nav');
    const navMobile = query(".nav");
    btnNavMobile && navMobile ? modal(btnNavMobile, navMobile) : 'No hay modal';

}
