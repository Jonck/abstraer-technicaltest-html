import Swiper from 'swiper/bundle'; 


const swiper = new Swiper('.swiper-galery', {
    slidesPerView: 1,
    spaceBetween: 100,
    //pagination
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});