// import { query, queryAll, getClass, getId } from '../base/selector';
import { validateData } from './validate';

/**
 * 
 * @param {HTML} Form 
 */
export const formStart = (formData) => {
    const bntSend = formData.querySelector('input[type="submit"]');
    const formArrayInput = Array.from(formData.querySelectorAll('.inputData'));
    let objectData = {};
    let valid;

    //validate input to input
    formArrayInput.forEach(input => {
        const key = input.name;
        objectData = {...objectData, [key]: false};

        input.addEventListener('blur', input => {
            objectData[key] = validateData(input.target);

            valid = Object.values(objectData).some(e => e == false);
            !valid ? bntSend.classList.remove('opacity-50') : bntSend.classList.add('opacity-50');
        });

    });

    
    // submit stop
    formData.addEventListener('submit', e => {
        e.preventDefault();
        valid ? console.log('existe por lo menos uno mal') : console.log('ok');
    })
}

