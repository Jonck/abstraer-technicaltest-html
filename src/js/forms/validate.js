import { machOK, matchErr } from './response'

/**
 * 
 * @param {HTML} input 
 */
export const validateData = input => {
    let res;
    switch (input.dataset.valid) {
        case 'email':
            const email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            res = email.test(input.value) ? machOK(input) : matchErr(input);
            break;
        case 'str':
            res = input != "" && input.value.length > 0 ? machOK(input) : matchErr(input);
            break;
        case 'strStrong':
            res = input != "" && input.value.length > 5 ? machOK(input) : matchErr(input);
            break;
        default:
            break;
    }
    return res;
}