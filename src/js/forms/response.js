/**
 * 
 * @param {HTML} input 
 */
export const machOK = input => {
    const err = input.parentNode.querySelector('.err');
    input.style.border = "2px solid green";
    err ? err.remove() : 'Elemento borrado';
    return input.value;
}

/**
 * 
 * @param {HTML} input 
 */
export const matchErr = input => {
    const err = input.parentNode.querySelector('.err');
    input.style.border = "2px solid red";
    err ? 'El mensaje ya esta creado' : createMessage(input.parentNode);
    return false;
}

/**
 * 
 * @param {HTML} input 
 */
function createMessage(parent) {
    const message = document.createElement('p');
    message.textContent = 'Este campo no es correcto';
    message.classList.add('err');
    parent.appendChild(message);
    return 'Elemento Creado'
}