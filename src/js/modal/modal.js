import {
    toggleClass
} from '../base/classHTML'

/**
 * 
 * @param {HTML} btn 
 * @param {HTML} modal 
 */
export function modal(btn, modal) {
    btn.addEventListener("click", e => {
        toggleClass(modal, 'inactiveModal');
        toggleClass(btn, 'btnNavActive');
    });
}