/**
 * @param {HTML} ElmentAdd 
 * @param {string} TextClass 
 */
export const addClass = (elmentAdd, textClass) => elmentAdd.classList.add(textClass),
    removeClass = (elmentAdd, textClass) => elmentAdd.classList.remove(textClass),
    toggleClass = (elmentAdd, textClass) => elmentAdd.classList.toggle(textClass);