/**
 *  * @param {string} Element  
 */
export const query = e => document.querySelector(e),
    queryAll = e => document.querySelectorAll(e),
    getClass = e => document.getElementsByClassName(e),
    getId = e => document.getElementsByClassName(e);
