/**
 * @param {string} name 
 * @param {string} data 
 * Set data in local storage
 */
export const inserStorage = (name, data)  => {
    localStorage.setItem(name, data)
    return `create key ${name}`
}
/**
 * @param {string} item 
 * GET DATA
 */
export const getStorage = item => localStorage.getItem(item)


/**
 * @param {string} item
 * Remove item to local storage
 */
export const removeStorage = item => {
    localStorage.removeItem(item)
    return `clear key ${item}`
}

/**
 * Clear local Sorage
 */
export const clearStorage = () => {
    localStorage.clear()
    return `clear local Storage`
}